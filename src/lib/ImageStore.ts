import { writable } from "svelte/store";

export interface ImageData {
  name: string;
  type: string;
  size: number;
  imageURL: string;
}

export const images = writable<Array<ImageData>>([]);
